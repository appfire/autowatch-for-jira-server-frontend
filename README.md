# README #

This is the modified source of the JIRA Automation plugin that was used for the server version of Autowatch plugin for JIRA.

[Original Code](https://bitbucket.org/atlassianlabs/atlassian-autowatch-plugin).

[Original Marketplace page](https://marketplace.atlassian.com/plugins/com.atlassian.addon.connect.jiraautowatch/cloud/overview).

[Original LICENSE](https://www.gnu.org/licenses/gpl.html).