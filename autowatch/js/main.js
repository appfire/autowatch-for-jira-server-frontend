/*require([
    "autowatch/lib/jquery",
    "autowatch/lib/underscore",
    "autowatch/lib/backbone",
    "autowatch/lib/connect",
    "autowatch/lib/restfultable",
    "autowatch/util/util",
    "autowatch/view/restfultable/jql",
    "autowatch/view/restfultable/users",
    "autowatch/view/restfultable/row",
    "autowatch/view/restfultable/editRow",
    "autowatch/model/restfultable/ruleModel"
],function($, _, Backbone, AP, RT, util, jqlView, usersView, AutowatchRuleRow, AutowatchRuleRowEdit, AutowatchRuleModel) {
*/
    // DOM ready
    AJS.$(function () {
        var url = AJS.Meta.get('context-path') + "/rest/autowatch/1.0/rule";
        var projectKey = AJS.$('meta[name=projectKey]').attr('content');
        var licenseValid = AJS.$('meta[name=license]').attr('content');
        var $rulesTable = AJS.$("#rules-table");

        // Display a message if the license is not valid.
        if (licenseValid === "false")
        {
            AJS.messages.error({
                title: "License error:",
                body: "Your license has expired or is invalid. Please purchase a license to continue using Autowatch for JIRA."
            });
        }

        // This is here to catch any permissions errors. The Restful table doesn't have handlers if permissions fail.
        AJS.$.ajax({
            url: url + "/project/" + projectKey,
            type: "GET",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(data) {
                // Do nothing, we good.
            },
            error: function (resp) {
              var data = JSON.parse(resp.responseText);
              var errorMessage = "";
              var errors;
              if(data.message) {
                  errors = [data.message];
              } else if (data.errorMessages) {
                  errors = data.errorMessages;
              } else {
                  errors = data;
              }
              AJS.$.each(errors, function (index, value) {
                  errorMessage += "* " + value + "\n";
              });

              AJS.messages.error("#aui-message-bar", {
                  title: 'Ran into errors while trying to fetch rules. Try logging out and logging back in.',
                  body: errorMessage
              });
            }
        });

        // Shows the dialog when the "Show dialog" button is clicked
        AJS.$("#about-button").click(function() {
            AJS.dialog2("#about-dialog").show();
        });

        var rulesTable = new AJS.RestfulTable({
            // Table element to render into.
            el: $rulesTable,

            // Autofocus first field, which provides immediate ability to create.
            autofocus: true,

            // Don't allow drag-n-drop reordering.
            allowReorder: false,

            // Rule model.
            model: AutowatchRuleModel,

            // RESTful resource hooks.
            resources: {
                all: url + "/project/" + projectKey, // resource to get all contacts
                self: url // resource to get single contact url/{id}
            },

            // Column defintions.
            columns: [
                {
                    id: "filter",
                    fieldName: "filter",
                    header: "JQL",
                    styleClass: "field-jql",
                    readView: jqlView.getRead(),
                    editView: jqlView.getEdit(),
                    emptyText: "Enter JQL filter"
                },
                {
                    id: "users",
                    fieldName: "users",
                    header: "Users",
                    styleClass: "field-users",
                    readView: usersView.getRead(),
                    editView: usersView.getEdit(),
                    emptyText: "Enter users"
                }
            ],

            // Custom view declarations.
            views: {
                row: AutowatchRuleRow,
                editRow: AutowatchRuleRowEdit
            },

            // Message to display when no autowatch rules have been defined.
            noEntriesMsg: "No autowatch rules have been specified.",

            // Callback to execute when user tries to delete. Return true to delete.
            /*
            deleteConfirmation: function() {
                return true;
            },*/

            // ???
            fieldFocusSelector: function(name) {
                return ":input[type!=hidden][name=" + name + "], #" + name + ", .ajs-restfultable-input-" + name;
            }
        });

        // SUPER ugly hack to ensure the first edited row doesn't somehow try to save on blur.
        // This behavior is built into the RestfulTable for some reason and it sucks.
        rulesTable._saveEditRowOnBlur = function() {
            return false;
        };

        AJS.$(rulesTable)
            .bind(AJS.RestfulTable.Events.INITIALIZED, util.resizeContainer)
            .bind(AJS.RestfulTable.Events.ROW_ADDED, util.resizeContainer)
            .bind(AJS.RestfulTable.Events.ROW_REMOVED, util.resizeContainer);
        AJS.$(window).scroll(util.resizeContainer);
    });
/*
});
*/
