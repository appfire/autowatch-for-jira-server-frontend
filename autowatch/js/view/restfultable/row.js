/*define([
    "autowatch/lib/jquery",
    "autowatch/lib/underscore",
    "autowatch/lib/backbone",
    "autowatch/lib/connect",
    "autowatch/lib/restfultable",
    "autowatch/util/util"
],function($, _, Backbone, AP, RT, util) {*/
    AutowatchRuleRow = AJS.RestfulTable.Row.extend({
        render: function() {
            var el = AJS.RestfulTable.Row.prototype.render.apply(this, Array.prototype.slice.call(arguments));
            if (this.model.get("noInvalidExecutions") > 0) {
                this.$el.addClass("invalid-rule");
            } else {
                this.$el.removeClass("invalid-rule");
            }
            return el;
        }
    });
/*
});
*/
