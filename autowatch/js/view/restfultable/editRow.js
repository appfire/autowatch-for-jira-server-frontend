/*define([
    "autowatch/lib/jquery",
    "autowatch/lib/underscore",
    "autowatch/lib/backbone",
    "autowatch/lib/connect",
    "autowatch/lib/restfultable",
    "autowatch/util/util"
],function($, _, Backbone, AP, RT, util) {*/
    AutowatchRuleRowEdit = AJS.RestfulTable.EditRow.extend({
        /*
        initialize: function() {
            AJS.RestfulTable.EditRow.prototype.initialize.apply(this, Array.prototype.slice.call(arguments));

            this.bind(AJS.RestfulTable.Events.FOCUS, util.resizeContainer);
        },*/
        serializeObject: function() {
            var data = {};

            this.$el.find(":input:not(:button):not(:submit):not(:radio):not('select[multiple]'):not([type=text].user-picker)").each(function () {

                if (this.name === "") {
                    return;
                }

                if (this.value === null) {
                    this.value = "";
                }

                data[this.name] = this.value.match(/^(tru|fals)e$/i) ?
                    this.value.toLowerCase() == "true" : this.value;
            });

            // select2 user picker override!
            this.$el.find("input[type=text].user-picker").each(function() {
                data[this.name] = AJS.$(this).select2("val");
            });

            this.$el.find("input:radio:checked").each(function(){
                data[this.name] = this.value;
            });

            this.$el.find("select[multiple]").each(function(){

                var $select = jQuery(this),
                    val = $select.val();

                if ($select.data("aui-ss")) {
                    if (val) {
                        data[this.name] = val[0];
                    } else {
                        data[this.name] = "";
                    }
                } else {

                    if (val !== null) {
                        data[this.name] = val;
                    } else {
                        data[this.name] = [];
                    }
                }
            });

            data.projectKey = AJS.$('meta[name=projectKey]').attr('content');
            var rule = this.model.get("rule");
            if (typeof rule !== "undefined")
            {
                data.id = this.model.get("rule").id;
            } else {
                data.id = undefined;
            }

            // Plumb a reference to the editRow.
            data.editRow = this;

            data.error = function(xhr) {
                var data = JSON.parse(xhr.responseText);
                var responseCode = xhr.status;

                if (responseCode == 400) {
                    title = "Error updating/adding rule:";
                    message = "<ul>";

                    // Errors can come from multiple places. Check and report all.
                    if (typeof data.triggerErrors.errors !== "undefined") {
                        var triggerError = data.triggerErrors.errors.jiraJqlExpression;
                        if (typeof triggerError !== "undefined") {
                            message += "<li>" + "JQL error: " + triggerError + "</li>";
                        }
                    }
                    if (typeof data.ruleErrors.errors !== "undefined") {
                        var ruleError = data.ruleErrors.errors.name;
                        if (typeof ruleError !== "undefined") {
                            if (ruleError === "Rule name is invalid") {
                                ruleError = "JQL field cannot be empty.";
                            }
                            else if (ruleError === "Rule with the same name already exists") {
                                ruleError = "You already have a rule with the same JQL. Please add watchers to the existing rule.";
                            }

                            message += "<li>" + "Rule error: " + ruleError + "</li>";
                        }
                    }
                    if (typeof data.actionErrors[0].errors !== "undefined") {
                        var actionError = data.actionErrors[0].errors.jiraAddWatchersField;
                        if (typeof actionError !== "undefined") {
                            if (actionError === "Field parameters cannot be empty") {
                                actionError = "You must add at least one watcher for an autowatch rule."
                            }

                            message += "<li>" + "Watcher list error: " + actionError + "</li>";
                        }
                    }
                    message += "</ul>";
                } else {
                    title = "Error performing autowatch rule update (" + responseCode + ")";
                    message = xhr.responseText;
                }

                // Clear out the message bar.
                var $bar = AJS.$("#aui-message-bar");
                $bar.empty();

                AJS.messages.error({
                    title: title,
                    body: message
                });
            };

            return data;
        }
    });

/*
});*/
