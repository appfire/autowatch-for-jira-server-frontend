/*define([
    "autowatch/lib/jquery",
    "autowatch/lib/underscore",
    "autowatch/lib/restfultable",
    "autowatch/lib/JQLAutoComplete",
    "autowatch/lib/connect"
], function($, _, RT, JQLAutoComplete, AP) {*/
    jqlView = {};
    jqlView.getRead = function () {
        return AJS.RestfulTable.CustomReadView.extend({
            render: function (self) {
                var icon;

                if (this.model.get("noInvalidExecutions") > 0) {
                    icon = AJS.$(aui.icons.icon({icon: "error", useIconFont: true}));
                    this.$el
//                        .attr("title", JSON.parse(this.model.get("invalidExecutionMessage")).error.errorMessages[0])
                        .attr("title", "There was error applying this autowatch rule.")
                        .tooltip({aria:true});
                    this.$el.append(icon);
                } else {
                    icon = AJS.$(aui.icons.icon({icon: "approve", useIconFont: true}));
                    this.$el.append(icon);
                }

                // Get the JQL from the rule.
                var rule = this.model.get("rule");
                if (typeof rule !== "undefined") {
                    var filter = rule.trigger.params.jiraJqlExpression[0];
                } else {
                    var filter = "";
                }

                this.$el.append(document.createTextNode( " " + filter ));

                return this.el;
            }
        });
    };

    jqlView.getEdit = function() {
        return AJS.RestfulTable.CustomEditView.extend({
            render: function (self) {
                var fieldId = 'jql-filter-' + this.model.cid;
                var $html = AJS.$(
                    '<div style="white-space:nowrap">' +
                        '<span class="jql-indicator aui-icon aui-icon-small"></span>' +
                        '<textarea class="text jql-filter" name="filter" id="' + fieldId + '"></textarea>' +
                    '</div>'
                );
                var $jql = $html.find("textarea");
                var $errorEl = $html.find("span");

                // Get the JQL from the rule.
                var rule = this.model.get("rule");
                if (typeof rule !== "undefined") {
                    var filter = rule.trigger.params.jiraJqlExpression[0];
                } else {
                    var filter = "";
                }

                $jql.val(filter); // select currently selected

                AJS.$.ajax({
                    url: AJS.Meta.get('context-path') + "/rest/api/2/jql/autocompletedata",
                    headers: {
                        "Accept": "application/json"
                    },
                    success: function(data) {
                        // Our data is already JSON.
                        //data = JSON.parse(data);

                        // to disable autocomplete of project field we filter it out from the array
                        data.visibleFieldNames = _.filter(data.visibleFieldNames, function(field) {
                            return field.value !== "project";
                        });

                        var jqlAutoComplete = JQLAutoComplete({
                            field: $jql,
                            parser: JQLAutoComplete.MyParser(data.jqlReservedWords),
                            queryDelay: 0.65,
                            jqlFieldNames: data.visibleFieldNames,
                            jqlFunctionNames: data.visibleFunctionNames,
                            minQueryLength: 0,
                            allowArrowCarousel: true,
                            autoSelectFirst: false,
                            errorEl: $errorEl
                        });
                        $jql.expandOnInput();

                        jqlAutoComplete.buildResponseContainer();
                        jqlAutoComplete.parse($jql.text());
                        jqlAutoComplete.updateColumnLineCount();

                        $jql.keypress(function(event) {
                            if (jqlAutoComplete.dropdownController === null || !jqlAutoComplete.dropdownController.displayed || jqlAutoComplete.selectedIndex < 0) {
                                if (event.keyCode == 13 && !event.ctrlKey && !event.shiftKey) {
                                    event.preventDefault();
                                    jqlAutoComplete.dropdownController.hideDropdown();
                                }
                            }
                        }).bind('expandedOnInput', function() {
                                jqlAutoComplete.positionResponseContainer();
                                //AP.resize(); // resize add-on container if suggestions wouldn't fit
                            }).click(function() {
                                jqlAutoComplete.dropdownController.hideDropdown();
                            });
                    }
                });

                return $html;
            }
        });
    };
/*
    return exports;
});*/
