/*define([
    "autowatch/lib/jquery",
    "autowatch/lib/restfultable"
],function($, RT) {*/
    AutowatchRuleModel = AJS.RestfulTable.EntryModel.extend({
        save: function (options, handlers) {
            options = options || {editRow: {}};

            var instance = options.editRow;
            var errorInstance = this;
            var baseUrl = AJS.Meta.get('context-path') + "/rest/autowatch/1.0/rule";

            // Get the current user.
            var currentUser = AJS.$('meta[name=ajs-remote-user]').attr('content');

            // Display a message if the license is not valid.
            var licenseValid = AJS.$('meta[name=license]').attr('content');
            if (licenseValid === "false")
            {
                // Clear out the message bar.
                var $bar = AJS.$("#aui-message-bar");
                $bar.empty();

                AJS.messages.error({
                    title: "Cannot save rule:",
                    body: "Your license has expired or is invalid. Please purchase a license to continue using Autowatch for JIRA."
                });

                options.editRow._submitFinished();
                return this;
            }

            // Make sure the user isn't using project-based queries in the JQL filter. If they have, display an error
            // message and abort out.
            var re = new RegExp("project\\s*(=|!=|~|<=|>=|>|<|!~|is not|is|not in|in|was|was not|was in)");
            if (typeof options.filter !== "undefined" && options.filter != null)
            {
                var projectSearchResults = options.filter.search(re);
                if (projectSearchResults!= -1)
                {
                    var projectKey = AJS.$('meta[name=projectKey]').attr('content');

                    // Clear out the message bar.
                    var $bar = AJS.$("#aui-message-bar");
                    $bar.empty();

                    AJS.messages.error({
                        title: "Error updating/adding rule:",
                        body: "<ul><li>" + "JQL error: You cannot specify project filters in your query. " +
                              "Your query is already scoped to project " + projectKey + ".</li></ul>"
                    });

                    options.editRow._submitFinished();
                    return this;
                }
            }

            var ruleJson = {
                "name": options.filter,
                "actor": currentUser,
                "project": options.projectKey,
                "trigger": {
                    "params": {
                        // Trigger on issue created (1) and issue updated (2)
                        "jiraEventId": ["1", "2"],

                        // Trigger on issues matching our desired JQL
                        "jiraJqlExpression": [options.filter],

                        // ???
                        "eventSyncProcessingOverride": ["none"],
                        "currentIsInGroup": ["true"]
                    },
                    "moduleKey": "com.mohamicorp.plugin.autowatch.autowatch-for-jira:issue-event-trigger"
                },
                "actions": [{
                    "params": {
                        // Add an array of watchers
                        "jiraAddWatchersField": options.users
                    },
                    "moduleKey": "com.mohamicorp.plugin.autowatch.autowatch-for-jira:add-watchers-action"
                }],
                "enabled": true
            };

            var successFunc = function (data) {
                data.id = data.rule.id;

                // Clear out the message bar since we successfully saved.
                var $bar = AJS.$("#aui-message-bar");
                $bar.empty();

                if (instance.isUpdateMode) {
                    // We've screwed with how Backbone views like to sync, so we need to update the model somehow.
                    // There's probably a better way to sync() this, but until we get that working, the easiest
                    // thing to do is to just manually update the model with the values we just saved.
                    errorInstance.attributes.rule.trigger.params.jiraJqlExpression = [options.filter];
                    errorInstance.attributes.rule.actions["0"].params.jiraAddWatchersField = options.users;

                    // Notify view that we have updated.
                    instance.trigger(instance._event.UPDATED, data, true);
                } else {
                    instance.trigger(instance._event.CREATED, data);
                    instance.model = new instance._modelClass(); // reset

                    instance.render({errors: {}, values: {}}); // pulls in instance's model for create row
                    instance.trigger(instance._event.FOCUS);
                }

                instance.trigger(instance._event.SUBMIT_FINISHED);
            };

            var errorFunc = function (xhr) {
                errorInstance._serverErrorHandler(xhr, this);
                if (options.error) {
                    options.error.call(instance, xhr);
                }

                options.editRow._submitFinished();
            };

            // Perform rule update. The ID check here is weird because we've fucked up the backbone model and this is
            // the only id reference that's currently consistent.
            if (typeof options.editRow.model.attributes.rule !== "undefined" &&
                typeof options.editRow.model.attributes.rule.id !== "undefined") {
                // Add our logged ID to the rule info.
                ruleJson["id"] = options.editRow.model.attributes.rule.id;

                AJS.$.ajax({
                    url: baseUrl + "/" + options.editRow.model.attributes.rule.id,
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify(ruleJson),
                    success: successFunc,
                    error: errorFunc
                });

            // Add new rule.
            } else {
                AJS.$.ajax({
                    url: baseUrl,
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(ruleJson),
                    success: successFunc,
                    error: errorFunc
                });
            }

            return this;
        },

        destroy: function (options) {

            var baseUrl = AJS.Meta.get('context-path') + "/rest/autowatch/1.0/rule";
            options = options || {};

            // Get the rule ID.
            var id = this.get("rule").id;

            // Get the project key.
            var projectKey = AJS.$('meta[name=projectKey]').attr('content');

            // Build the REST URL.
            var instance = this;

            AJS.$.ajax({
                url: baseUrl + "/project/" + projectKey + "/" + id,
                type: "DELETE",
                dataType: "json",
                success: function (data) {
                    if(instance.collection){
                        instance.collection.remove(instance);
                    }
                    if (options.success) {
                        options.success.call(instance, data);
                    }
                },
                error: function (xhr) {
                    instance._serverErrorHandler(xhr, this);
                    if (options.error) {
                        options.error.call(instance, xhr);
                    }

                    options.editRow._submitFinished();
                }
            });

            return this;
        }
    });

/*
});*/
